#include <iostream>
#include <iomanip>
#include <cmath>

#include <thread>
#include <chrono>

#include "lacze_do_gnuplota.hh"
#include "bryla.hh"
#include "scena.hh"

using namespace std;

int main()
{
  PzG::LaczeDoGNUPlota Lacze;
  // char c;
  Lacze.Inicjalizuj();
  Scena<float, 16, 43, 25> scena = Scena<float, 16, 43, 25>();
  Wektor<float> przesuw = Wektor<float>();
  // Bryla<float, 43> woda = Bryla<float, 43>();
  // Bryla<float, 25> podloze = Bryla<float, 25>();
  // Bryla<float, 16> dron = Bryla<float, 16>();
  scena.getpodloze().Eksportzpliku("bryly/powierzchnia.dat");
  scena.getwoda().Eksportzpliku("bryly/woda.dat");
  scena.getdron().Eksportzpliku("bryly/dron.dat");
  scena.getdron().Importdopliku("bryly/dron1.dat");
  scena.rysuj();

  char wybor = '\0';
  while (wybor != 'k')
  {
    cout << "r - zadaj ruch na wprost \no - zadaj zmiane orientacji \nm - wyswietl menu\nk - koniec dzialania programu\n " << endl;
    cin >> wybor;
    switch (wybor)
    {
    case 'r':
    {
      Wektor<float> mov = Wektor<float>();
      cout << "Wpisz wektor ruchu: \n";
      mov.wpiszwektor();
      cout << przesuw << endl;
      cout << mov << endl;
      for (int i = 0; i < 3; i++)
      {
        przesuw[i] = przesuw[i]+mov[i];
      }
      cout << przesuw << endl;
      scena.getdron().getprzesuniecie() = przesuw;
      for (int i = 0; i < 3; i++)
      {
        mov[i] = mov[i]/20;
      }
      

      for (int i = 0; i < 20; i++)
      {
        scena.getdron().przesun(mov);
        scena.getdron().Importdopliku("bryly/dron1.dat");
        scena.rysuj();

        // spanko
        chrono::milliseconds timespan(100); // or whatever
        this_thread::sleep_for(timespan);
      }
      break;
    }

    case 'o':
    {
      double kat;
      cout << "Wpisz kat obrotu \n";
      cin >> kat;
      double radian = (3.14*kat)/(180);
      // for (int i = 0; i < 20; i++)
      // {
        scena.getdron().obrot(radian);
        scena.getdron().Importdopliku("bryly/dron1.dat");
        scena.rysuj();

      //   // spanko
      //   chrono::milliseconds timespan(100); // or whatever
      //   this_thread::sleep_for(timespan);
      // }

      // double kat;
      // cout << "Wpisz kat obrotu \n";
      // cin >> kat;

      // for (int i = 0; i < 20; i++)
      // {
      //   scena.getdron().obrot(kat);
      //   scena.getdron().Importdopliku("bryly/dron1.dat");
      //   scena.rysuj();

      //   // spanko
      //   chrono::milliseconds timespan(100); // or whatever
      //   this_thread::sleep_for(timespan);
      // }
      break;
    }

    case 'm':
      break;

    case 'k':
      while (1)
        return 0;

    default:
      break;
    }
  }
  return 0;
}

//   cout << "Podaj wartosc kata (wznoszenia/opadania) w stopniach. \n\tWartosc kata:  ";
//   int kat;
//   cin >> kat;
//   cout << kat << endl;
//   cout << "Podaj wartosc odleglosci, na ktora ma sie przemiescic dron. \n\tWartosc odleglosci:  ";
//   int odl;
//   cin >> odl;
//   cout << odl << endl;
//   float tang = tan(kat)/odl;
//   for (int i = 0; i < 20; i++)
// {
//   dron.przesun(odl / 20, 0 / 20, tang/20);
//   dron.przeslijBryledopliku("bryly/dron1.dat");
//   Lacze.UsunWszystkieNazwyPlikow();
//   Lacze.DodajNazwePliku("bryly/dron1.dat");
//   Lacze.Rysuj();

//   // spanko
//   chrono::milliseconds timespan(100); // or whatever
//   this_thread::sleep_for(timespan);
// }