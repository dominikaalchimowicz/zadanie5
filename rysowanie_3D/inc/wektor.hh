#ifndef WEKTOR_HH
#define WEKTOR_HH

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std; 
template <class Typ>
class Wektor
{
    Typ wektor[3];

public:
    Wektor()
    {
        for (int i = 0; i < 3; i++)
            wektor[i] = 0;
    }
    // operatory indeksujace (dzieki nim mozna operowac na prywatnej czesci klasy)
    Typ operator[](int Ind) const { return wektor[Ind]; }
    Typ &operator[](int Ind) { return wektor[Ind]; }

    void wpiszwektor()
    {
        for (int i = 0; i < 3; i++)
            cin >> wektor[i];
    }
};
template <class Typ>
std::ostream &operator<<(std::ostream &strm, const Wektor<Typ> &wektor)
{
  for (int i = 0; i < 3; i++)
  {
    strm << wektor[i] << " ";
  }
  strm << endl;
  return strm;
}
template <class Typ>
Typ operator*(Wektor<Typ> pom, Wektor<Typ> tmp)
{
  Typ k, j;
  k = 0;
  j = 0;
  for (int i = 0; i < 3; i++)
  {
    j = pom[i] * tmp[i];
    k = k + j;
  }
  return k;
}
#endif