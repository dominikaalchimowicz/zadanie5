#ifndef MACIERZ_HH
#define MACIERZ_HH

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include "wektor.hh"
using namespace std;

template <class Typ>
class Macierz
{
    Wektor<Typ> macierz[3];

public:
    // operatory indeksujace (dzieki nim mozna operowac na prywatnej czesci klasy)
    Wektor<Typ> operator[](int Ind) const { return macierz[Ind]; }
    Wektor<Typ> &operator[](int Ind) { return macierz[Ind]; }
    Macierz()
    {

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                macierz[i][j] = 0;
            }
            macierz[i][i] = 1;
        }
    }
    void copy(Macierz from)
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                macierz[i][j] = from[i][j];
            }
        }
    }
};
template <class Typ>
Macierz<Typ> operator*(Macierz<Typ> pom, Macierz<Typ> tmp)
{
    Macierz<Typ> wynik = Macierz<Typ>();
    float a, b;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            for (int k = 0; k < 3; k++)
            {

                a = pom[i][k] * tmp[k][j];
                b = b + a;
            }
            wynik[i][j] = b;
            a = 0, b = 0;
        }
    }
    return wynik;
}
template <class Typ>
Wektor<Typ> operator*(Macierz<Typ> macierz, Wektor<Typ> wektor)
{
    Wektor<Typ> wynik = Wektor<Typ>();
    Typ b, a;
    b = 0;
    a = 0;
    for (int i = 0; i < 3; i++)
    {
        b = macierz[i] * wektor;
        wynik[i] = b;
    }
    return wynik;
}

template <class Typ>
std::ostream &operator<<(std::ostream &out, const Macierz<Typ> &macierz)
{
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            cout << macierz[i][j] << " ";
        }
        cout << endl;
    }
    return out;
}

#endif