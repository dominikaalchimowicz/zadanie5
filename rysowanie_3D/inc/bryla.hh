#ifndef BRYLA_HH
#define BRYLA_HH

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include "macierz.hh"
#include "wektor.hh"
using namespace std;

template <class Typ, int rozmiar>
class Bryla : public Macierz<Typ>
{
    Wektor<Typ> bryla[rozmiar];
    Macierz<Typ> macierzobrotu;
    Wektor<Typ> przesuniecie;

public:
    // operatory indeksujace (dzieki nim mozna operowac na prywatnej czesci klasy)
    Wektor<Typ> operator[](int Ind) const { return bryla[Ind]; }
    Wektor<Typ> &operator[](int Ind) { return bryla[Ind]; }
    Macierz<Typ> getmacierzobrotu() const { return macierzobrotu; }
    Macierz<Typ> &getmacierzobrotu() { return macierzobrotu; }
    Wektor<Typ> getprzesuniecie() const { return przesuniecie; }
    Wektor<Typ> &getprzesuniecie() { return przesuniecie; }
    Bryla()
    {

        for (int i = 0; i < rozmiar; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                bryla[i][j] = 0;
            }
        }
    }
    void Importdopliku(string sciezka)
    {
        ofstream zapis(sciezka);
        for (int i = 0; i < rozmiar; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                zapis << bryla[i][j] << " ";
            }
            zapis << endl;
        }
        zapis.close();
    }
    void wpiszwierzcholki()
    {
        for (int i = 0; i < rozmiar; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                cin >> bryla[i][j];
            }
        }
    }

    void przesun(Wektor<Typ> wektor)
    {
        for (int i = 0; i < rozmiar; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                bryla[i][j] += wektor[j];
            }
        }
    }

    void Eksportzpliku(string plik)
    {
        ifstream file;
        file.open(plik.c_str());
        for (int i = 0; i < rozmiar; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                file >> bryla[i][j];
            }
        }
    }

    void obrot(double radian)
    {
        Macierz<Typ> pom = Macierz<Typ>();
        pom.copy(macierzobrotu);
        // double radian = (3.14*kat)/180;
        pom[0][1] = -1 * sin(radian);
        pom[1][0] = sin(radian);
        pom[1][1] = cos(radian);
        pom[0][0] = pom[1][1];
        pom[2][2] = 1;
        cout << " macierz obrotu o prostu" << endl;
        cout << macierzobrotu << endl;
        macierzobrotu = macierzobrotu * pom;
        cout << " macierz obrotu o pomnozeniu razy kat obrotu" << endl;
        cout << macierzobrotu << endl;
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (macierzobrotu[i][j] <= 0.001 && macierzobrotu[i][j] >= -0.001)
                    macierzobrotu[i][j] = 0;
                if (macierzobrotu[i][j] >= 0.999)
                    macierzobrotu[i][j] = 1;
                if (macierzobrotu[i][j] <= -0.999)
                    macierzobrotu[i][j] = -1;
            }
        }
        cout << " macierz obrotu po pozbyciu sie strasznych liczb" << endl;
        cout << macierzobrotu << endl;
        cout << "przesuniecie" << endl;
        cout << przesuniecie << endl;
        for (int i = 0; i < 3; i++)
        {
            przesuniecie[i] = -1 * przesuniecie[i];
        }
        cout << "przesuniecie z -" << endl;
        cout << przesuniecie << endl;
        przesun(przesuniecie);
        Wektor<Typ> tmp = Wektor<Typ>();
        for (int i = 0; i < rozmiar; i++)
        {
            tmp = macierzobrotu * bryla[i];
            for (int j = 0; j < 3; j++)
            {
                bryla[i][j] = tmp[j];
            }
        }
        for (int i = 0; i < 3; i++)
        {
            przesuniecie[i] = -1 * przesuniecie[i];
        }
        cout << "przesuniecie z +" << endl;
        cout << przesuniecie << endl;
        przesun(przesuniecie);

        // for (int i = 0; i < 3; i++)
        // {
        //     pom = macierzobrotu;
        //     for (int j = 0; j < 3; j++)
        //     {
        //         bryla[i][j] = tmp[j]
        //     }
        // }
        cout << " macierz obrotu pomnozona * kad i mnozona razy wspolrzedne" << endl;
        cout << macierzobrotu * bryla[1] << endl;

        // Wektor<Typ> tmp = Wektor<Typ>();
        // for (int i = 0; i < rozmiar; i++)
        // {
        //     tmp = macierzobrotu * bryla[i];
        //     for (int j = 0; j < 3; j++)
        //     {
        //         bryla[i][j] = tmp[j];
        //     }
        // }
    }
};

#endif