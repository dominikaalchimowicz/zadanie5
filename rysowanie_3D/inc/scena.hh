#include <iostream>
#include <iomanip>
#include "bryla.hh"
#include "lacze_do_gnuplota.hh"
template <class Typ, int rozmiar1, int rozmiar2, int rozmiar3>
class Scena : public Bryla<Typ, rozmiar1 || rozmiar2 || rozmiar3>
{
    Bryla<Typ, rozmiar1> dron[rozmiar1];
    Bryla<Typ, rozmiar2> woda[rozmiar2];
    Bryla<Typ, rozmiar3> podloze[rozmiar3];
    PzG::LaczeDoGNUPlota Lacze;

public:
    Bryla<Typ, rozmiar1> getdron() const { return dron; }
    Bryla<Typ, rozmiar1> &getdron() { return *dron; }
    Bryla<Typ, rozmiar2> getwoda() const { return woda; }
    Bryla<Typ, rozmiar2> &getwoda() { return *woda; }
    Bryla<Typ, rozmiar3> getpodloze() const { return podloze; }
    Bryla<Typ, rozmiar3> &getpodloze() { return *podloze; }
    void rysuj()
    {
        Lacze.Rysuj();
    }

    Scena()
    {
        Lacze.ZmienTrybRys(PzG::TR_3D);
        Lacze.UstawZakresX(-100, 100);
        Lacze.UstawZakresY(-100, 100);
        Lacze.UstawZakresZ(-10, 100);
        Lacze.UstawRotacjeXZ(60, 40); // Tutaj ustawiany jest widok
        Lacze.UsunWszystkieNazwyPlikow();
        Lacze.DodajNazwePliku("bryly/powierzchnia.dat");
        Lacze.DodajNazwePliku("bryly/woda.dat");
        Lacze.DodajNazwePliku("bryly/dron1.dat");
    }
};